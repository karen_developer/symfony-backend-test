### Setup and running web application
1. `mkdir symfony-test`
1. `cd symfony-test`
1. `git clone git@gitlab.com:karen_developer/symfony-3-backend-test.git .`
1. `composer install`
1. `./bin/phpunit` - for starting Unit tests
1. `./bin/console server:run` - running the local server

### For testing

1. `curl -s -H "Content-Type: application/json" -d @request.json http://127.0.0.1:8000/`

<?php

namespace App\Service\Text\Processor;

interface TextProcessorInterface {

    /**
     * @return mixed
     */
    public function getMethods();

    /**
     * @param $method
     * @param $text
     * @return mixed
     */
    public function process($method, $text);
}

<?php

namespace App\Service\Text;

use App\Service\Text\Processor\TextProcessorInterface;

interface TextProcessorRegistry {

    /**
     * @param TextProcessorInterface $processor
     * @return mixed
     */
    public function register(TextProcessorInterface $processor);

    /**
     * @param $method
     * @param $text
     * @return mixed
     */
    public function process($method, $text);
}
